package LucroConfig;
import sistema.Main;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;


public class LucroConfig extends JDialog {

    private JPanel AdLucroPanel;
    private JFormattedTextField PInserida;
    private JButton confirmarButton;
    static private double porcentagem;
    private boolean actionperformed = false;

    Loader NewScreen = new Loader();

    public void InicializarJanela(){
        NewScreen.run();
    }

    //Swingworker para poder construir a tela de LucroConfig apartir de um event de um botão.
    public class Loader extends SwingWorker<Object, Object> {

        @Override
        protected Object doInBackground() throws Exception {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    getDialog().setTitle("Lucro");
                    getDialog().add(getPanel());
                    getDialog().pack();
                    getDialog().setResizable(false);
                    getDialog().setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
                    getDialog().setVisible(true);
                }
            });

            return null;
        }

        @Override
        protected void done() {
                System.out.println("Tela de Lucro construida.");
        }

    }//Final do loader

    //Getters
    public double getPorcentagem() {
        return porcentagem;
    }

    public boolean getActionperformed(){
        return actionperformed;
    }

    public JPanel getPanel(){
        return AdLucroPanel;
    }

    public JDialog getDialog(){
        return this;
    }

    public LucroConfig() {
        confirmarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try{//Tetar fazer a converção de string para double.

                    porcentagem = Double.parseDouble(PInserida.getText());
                    actionperformed = true;

                    try{
                        //Bloco de instruções para escrever a nova % de lucro dentro do arquivo.

                        BufferedWriter Escrever = new BufferedWriter(new FileWriter(".\\Margem De Lucro\\MargemDeLucro.txt"));
                        Escrever.write(String.valueOf(getPorcentagem()));
                        Escrever.close();
                        Main.Control.MudarTextoL(String.valueOf(getPorcentagem()));
                        Main.TelaPrinc.run();

                    }catch (Exception e){
                        System.out.println(e);
                    }


                    dispose();//fechando a tela.
                }catch (NumberFormatException e){
                    JOptionPane.showMessageDialog(null, "Insira um numero.", "Erro: ", JOptionPane.ERROR_MESSAGE);
                    System.out.println("Number Exeption: " + e);
                }
            }
        });
    }


}//Final da classe de LucroConfig
