package sistema;

//Herdeira da clase veiculo e setando os seus metodos e atributos
public class Moto implements Veiculo{
    private int Combustivel;
    private int Rendimento;
    private int MaxCarg;
    private int VelMed;
    double RendLoss;

    public Moto() {
        Combustivel = 5;
        Rendimento = 50;
        MaxCarg = 50;
        VelMed = 110;
        RendLoss = 0.3;
    }

    public int getCombustivel(){
        return Combustivel;
    }

    public int getRendimento(){
        return Rendimento;
    }

    public int getMaxCarg(){
        return MaxCarg;
    }

    public int getVelMed(){
        return VelMed;
    }

    public double getRendLoss(){
        return RendLoss;
    }
}
