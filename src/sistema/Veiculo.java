package sistema;

//Classe abstrata para os veiculos.

public interface Veiculo {

    int getCombustivel();

    int getRendimento();

    int getMaxCarg();

    int getVelMed();

    double getRendLoss();
}


