package sistema;
//Herdeira da clase veiculo e setando os seus metodos e atributos
public class Van implements Veiculo{
    private int Combustivel;
    private int Rendimento;
    private int MaxCarg;
    private int VelMed;
    double RendLoss;

    public Van() {
        Combustivel = 2;
        Rendimento = 10;
        MaxCarg = 3500;
        VelMed = 80;
        RendLoss = 0.001;
    }

    public int getCombustivel(){
        return Combustivel;
    }

    public int getRendimento(){
        return Rendimento;
    }

    public int getMaxCarg(){
        return MaxCarg;
    }

    public int getVelMed(){
        return VelMed;
    }

    public double getRendLoss(){
        return RendLoss;
    }

}
