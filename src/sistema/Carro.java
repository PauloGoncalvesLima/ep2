package sistema;
//Herdeira da clase veiculo e setando os seus metodos e atributos
public class Carro implements Veiculo {
    private int Combustivel;
    private int Rendimento;
    private int MaxCarg;
    private int VelMed;
    private double RendLoss;

    public Carro() {

        Combustivel = 5;
        Rendimento = 14;
        MaxCarg = 360;
        VelMed = 100;
        RendLoss = 0.025;
    }

    public int getCombustivel(){
        return Combustivel;
    }

    public int getRendimento(){
        return Rendimento;
    }

    public int getMaxCarg(){
        return MaxCarg;
    }

    public int getVelMed(){
        return VelMed;
    }

    public double getRendLoss(){
        return RendLoss;
    }
}
