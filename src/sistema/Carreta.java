package sistema;
//Herdeira da clase veiculo e setando os seus metodos e atributos
public class Carreta implements Veiculo{

    private int Combustivel;
    private int Rendimento;
    private int MaxCarg;
    private int VelMed;
    double RendLoss;

    public Carreta() {
        Combustivel = 2;
        Rendimento = 8;
        MaxCarg = 30000;
        VelMed = 60;
        RendLoss = 0.0002;
    }
    public int getCombustivel(){
        return Combustivel;
    }

    public int getRendimento(){
        return Rendimento;
    }

    public int getMaxCarg(){
        return MaxCarg;
    }

    public int getVelMed(){
        return VelMed;
    }

    public double getRendLoss(){
        return RendLoss;
    }
}
