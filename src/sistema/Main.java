package sistema;

import LucroConfig.LucroConfig;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;



public class Main {

    static public GUIControl Control;
    static LucroConfig Lucro;
    static JFrame TelaPrincipal;
    private static String OS = System.getProperty("os.name").toLowerCase();


    static public Thread TelaPrinc = new Thread(new Runnable() { public void run() {//Thread para poder iniciar a tela em uma outra classe.
        TelaPrincipal.setContentPane(Control.getMainPanel());
        TelaPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        TelaPrincipal.pack();
        TelaPrincipal.setResizable(false);
        TelaPrincipal.setVisible(true);
    }});



    public static void main(String[] args) {

        if (isWindows()) {
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            } catch (Exception e) {
                System.out.println(e);
            }
        }

         //Comando para centralizar o frame na tela.
        TelaPrincipal = new JFrame("Sistema de Fretes");
        TelaPrincipal.setLocationRelativeTo(null);
        Control = new GUIControl();
        Lucro = new LucroConfig();


        //Criando um novo arquivo de Margem de Lucro caso não tenha.
        File MDL = new File(".\\Margem De Lucro\\MargemDeLucro.txt");
        try {
            MDL.createNewFile();
        }catch (Exception e){
            System.out.println(e);
        }


        //Crindo um novo arquivo para salvar os veiculos caso não tenha.
        File VS = new File(".\\VeiculosSalvos\\Veiculos.txt");
        try {
            VS.createNewFile();
        }catch (Exception e){
            System.out.println(e);
        }


        try {

            BufferedReader VeiculosSalvos = new BufferedReader(new FileReader(VS.getAbsolutePath()));//Leitor do arqivo dos veiculos salvos.
            String LinhaLida;

            while ((LinhaLida = VeiculosSalvos.readLine()) != null) {

                String[] CurrVeic = LinhaLida.split("\\s"); // \\s é para se dar split em qualquer espaço em branco seja espaço ou tab.
                Control.addInList(CurrVeic[0]);//Adicionando veiculos salvos no arquivo VS na lista principal do programa.

            }
            VeiculosSalvos.close();
        }
        catch (Exception e){
            System.out.println(e);
        }

        //Bloco para inicializar a tela principal.
        try{

            BufferedReader MargemDeLucro = new BufferedReader(new FileReader(MDL.getAbsolutePath()));//Leitor para ler a margem de lucro salva.

            String LinhaLida;
            LinhaLida = MargemDeLucro.readLine(); //Lendo somente a primeira linha

            if(LinhaLida == null){

                Lucro.InicializarJanela();

            }
            else {
                Control.MudarTextoL(LinhaLida);//Função para mudar a label de porcentagem de lucro.
                TelaPrinc.run();
            }

            MargemDeLucro.close();//Fechando o bufferedReader.
        }catch (Exception e){
            System.out.println(e);
        }


    }//Final da main.


    public static boolean isWindows() {

        return (OS.indexOf("win") >= 0);

    }

}//Final da class Main.
