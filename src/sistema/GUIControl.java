    package sistema;

    import AdicionarFrete.AdFrete;
    import AdicionarVeiculos.AdicionarVeiculo;
    import LucroConfig.LucroConfig;
    import RemoveFrete.RMFrete;
    import RemoverVeiculo.RemoverVeiculo;

    import javax.swing.*;
    import javax.swing.table.DefaultTableModel;
    import java.awt.event.ActionEvent;
    import java.awt.event.ActionListener;
    import java.io.BufferedReader;
    import java.io.File;
    import java.io.FileReader;
    import java.util.ArrayList;

    public class GUIControl {

        private int Carro = 0, Carreta = 0, Van = 0, Moto = 0;

        private JButton adicionarVeiculoButton;
        private JButton removerVeiculoButton;
        private JList ListaDisponiveis;
        private JPanel MainPanel;
        private JButton alterarMargemDeLucroButton;
        private JButton removerFreteButton;
        private JButton adicionarFreteButton;
        private JTable TableDeFretes;
        private JLabel Lucrolabel;

        public int getCarro() {
            return Carro;
        }

        public int getCarreta() {
            return Carreta;
        }

        public int getVan() {
            return Van;
        }

        public int getMoto() {
            return Moto;
        }

        public JTable getTableDeFretes() {
            return TableDeFretes;
        }

        public double getLucro() {
            return Double.parseDouble(Lucrolabel.getText().split("%")[0]);
        }

        public JPanel getMainPanel() {
        return MainPanel;
    }

        private void updateMainList(){
            DefaultListModel DefModel = new DefaultListModel();
            DefModel.addElement("Carro  " + "x"+Carro);
            DefModel.addElement("Carreta  " + "x"+Carreta);
            DefModel.addElement("Van  " + "x"+Van);
            DefModel.addElement("Moto  " + "x"+Moto);

            ListaDisponiveis.setModel(DefModel);
        }

        public void addInTable(String Veiculo, String Distancia, String PesoDaCarga, String TempoDeEntrega, String Preco, String Lucro){

            DefaultTableModel DefModelT = (DefaultTableModel)TableDeFretes.getModel();

            Distancia+=" Km";
            PesoDaCarga+=" Kg";
            Lucro = "R$ " + Lucro;
            String NewData[] = {Veiculo, Distancia, PesoDaCarga, TempoDeEntrega, Preco, Lucro};

            DefModelT.addRow(NewData);
            TableDeFretes.setModel(DefModelT);

        }

        public void addInList(String Veiculo){

            if(Veiculo.equals("Carro")) Carro++;
            else if(Veiculo.equals("Carreta")) Carreta++;
            else if(Veiculo.equals("Van")) Van++;
            else if (Veiculo.equals("Moto")) Moto++;
            updateMainList();

        }

        public boolean rmInList(String Veiculo){

            if(Veiculo.equals("Carro") && Carro-1>=0){
                Carro--;
                if(Carro<0) Carro = 0;
                updateMainList();
                return true;

            }
            else if(Veiculo.equals("Carreta") && Carreta-1>=0){

                Carreta--;
                if(Carreta<0) Carreta = 0;
                updateMainList();
                return true;

            }
            else if(Veiculo.equals("Van") && Van-1>=0){

                Van--;
                if(Van<0) Van = 0;
                updateMainList();
                return true;

            }
            else if (Veiculo.equals("Moto") && Moto-1>=0){

                Moto--;
                if(Moto<0) Moto = 0;
                updateMainList();
                return true;

            }

            return false;

        }


        public void MudarTextoL(String NovoLucro){
                Lucrolabel.setText(NovoLucro+"% de Lucro");
        }

        public GUIControl() {

            updateMainList();

            String[] columns = {"Veículo","Distância", "Peso da carga", "Tempo de entrega", "Preço", "Lucro"};

            DefaultTableModel model = new DefaultTableModel(columns, 0);

            TableDeFretes.setModel(model);

            TableDeFretes.setSelectionModel(new ForcedListSelectionModel());


            File fretesSalvos = new File(".\\VeiculosSalvos\\FretesSalvos.txt");

            try{

                fretesSalvos.createNewFile();

            }catch (Exception err){
                System.out.println(err);
            }

            DefaultTableModel DefModelT = (DefaultTableModel)TableDeFretes.getModel();
            String Linha;

            try {
                BufferedReader Leitor = new BufferedReader(new FileReader(fretesSalvos.getAbsolutePath()));

                while ((Linha = Leitor.readLine()) != null){

                    columns = Linha.split("\t");
                    DefModelT.addRow(columns);
                    TableDeFretes.setModel(DefModelT);

                }

            }catch (Exception e){

                System.out.println(e);

            }


            adicionarVeiculoButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    AdicionarVeiculo NovoVeiculo = new AdicionarVeiculo();
                    NovoVeiculo.InicializarJanela();

                }
            });
            removerVeiculoButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    RemoverVeiculo tirarVeiculo = new RemoverVeiculo();
                    tirarVeiculo.InicializarJanela();

                }
            });
            alterarMargemDeLucroButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    try{

                        LucroConfig NovoLucro = new LucroConfig();
                        NovoLucro.InicializarJanela();

                    }catch (Exception e){

                        System.out.println(e);

                    }
                }
            });
            adicionarFreteButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    AdFrete NovoFrete = new AdFrete();
                    NovoFrete.InicializarJanela();

                }
            });

            removerFreteButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    RMFrete removerFrete = new RMFrete();
                    removerFrete.iniciarJanela();

                }
            });
        }

        public class ForcedListSelectionModel extends DefaultListSelectionModel {

            public ForcedListSelectionModel () {
                setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            }

            @Override
            public void clearSelection() {
            }

            @Override
            public void removeSelectionInterval(int index0, int index1) {
            }

        }

    }
