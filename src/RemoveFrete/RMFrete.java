package RemoveFrete;

import sistema.Main;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;


public class RMFrete extends JDialog{
    private JTable table1;
    private JButton confirmarButton;
    private JButton cancelarButton;
    private JPanel PainelPrincipal;


    public class Loader extends SwingWorker<Object, Object> {

        @Override
        protected Object doInBackground() throws Exception {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    getDialog().setTitle("Remover um frete.");
                    getDialog().add(getPanel());
                    getDialog().pack();
                    getDialog().setResizable(false);
                    getDialog().setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                    getDialog().setVisible(true);
                }
            });

            return null;
        }

        @Override
        protected void done() {
            System.out.println("Tela de Remover Frete construida.");

        }

    }

    public RMFrete() {

        table1.setModel(Main.Control.getTableDeFretes().getModel());//Pegando o modelo da tabela principal dos fretes.
        table1.setSelectionModel(new ForcedListSelectionModel());//Setando o modo de seleção para somente uma linha.

        confirmarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

               int index = table1.getSelectedRow();//Linha selecionada pelo usuario.

               if(index > -1){//Quando o index é igual a -1 quer dizer que o usuario não selecionou nenhuma linha da tabela.

                   DefaultTableModel newModel = (DefaultTableModel)Main.Control.getTableDeFretes().getModel();//Pegando o modelo da tabela principal e transformando ele em um defaultmodel

                   String[] linhaDaTabela = {"1", "2", "3","4","5","6"};//Inicializnado uma stringo com 6 espaços.

                   String freteASerRemovido = "";

                   ArrayList<String> linhasLidas = new ArrayList<>();




                   for(int i = 0; i < 6;i++){//Passando por cada coluna da linha selecionada pelo usuario.

                       linhaDaTabela[i] = table1.getValueAt(index, i).toString();

                       if(i > 0) linhaDaTabela[i] = "\t" + linhaDaTabela[i];//Igualando a forma de como esta salvo no arquivo.

                       freteASerRemovido+=linhaDaTabela[i];

                   }
                   //System.out.println(freteASerRemovido);
                   try{

                       BufferedReader leitor = new BufferedReader(new FileReader(".\\VeiculosSalvos\\FretesSalvos.txt"));
                       String linhaLida;
                       while ((linhaLida = leitor.readLine())!= null){
                           linhasLidas.add(linhaLida);
                       }

                       leitor.close();

                       if(linhasLidas.contains(freteASerRemovido)){

                           int indexx = linhasLidas.indexOf(freteASerRemovido);
                           //System.out.println(indexx);
                           linhasLidas.remove(indexx);

                       }

                       try{

                           BufferedWriter escritor = new BufferedWriter(new FileWriter(".\\VeiculosSalvos\\FretesSalvos.txt"));
                           for(String f : linhasLidas){

                               escritor.write(f);
                               escritor.newLine();

                           }

                           escritor.close();

                           String Linha;
                           ArrayList<String> Veiculos = new ArrayList<>();

                           try {

                               BufferedReader Leitor = new BufferedReader(new FileReader(".\\VeiculosSalvos\\Veiculos.txt"));

                               while ((Linha = Leitor.readLine()) != null){
                                   Veiculos.add(Linha);
                               }

                           }catch (Exception e){
                               System.out.println(e);
                           }

                           Veiculos.add(linhaDaTabela[0].split(" ")[0] + " 1");

                           try {

                               BufferedWriter Writer = new BufferedWriter(new FileWriter(".\\VeiculosSalvos\\Veiculos.txt"));
                               for(String v : Veiculos){
                                   Writer.write(v);
                                   Writer.newLine();
                               }

                               Writer.close();

                           }catch (Exception e){

                               System.out.println(e);

                           }

                           Main.Control.addInList(linhaDaTabela[0].split(" ")[0]);

                       }catch (Exception e){

                           System.out.println(e);

                       }

                   }catch (Exception e){
                       System.out.println(e);
                   }


                   newModel.removeRow(index);

                   Main.Control.getTableDeFretes().setModel(newModel);

                   table1.setModel(newModel);

               } else{

                   JOptionPane.showMessageDialog(null, "Escolha pelo menos uma linha para retirar.", "Erro", JOptionPane.ERROR_MESSAGE);

               }


            }
        });
        cancelarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                dispose();
            }
        });
    }

    public JPanel getPanel(){
        return PainelPrincipal;
    }

    public JDialog getDialog(){
        return this;
    }


    public void iniciarJanela(){
        Loader NewScreen = new Loader();
        NewScreen.run();
    }



    public class ForcedListSelectionModel extends DefaultListSelectionModel {

        public ForcedListSelectionModel () {
            setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        }

        @Override
        public void clearSelection() {
        }

        @Override
        public void removeSelectionInterval(int index0, int index1) {
        }

    }
}
