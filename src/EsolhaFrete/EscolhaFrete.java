package EsolhaFrete;

import sistema.Main;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class EscolhaFrete extends JDialog{
    private JPanel PanelPrincipal;

    private JRadioButton BMenorTempo;
    private JRadioButton BMenorCusto;
    private JRadioButton BCustoBeneficio;

    private JButton confirmarButton;

    private double peso;
    private double tempo;
    private double distancia;

    private double melhorTempo;
    private double melhorTCusto;
    private double maiorCusto;

    private double maiorCTempo;
    private double menorCusto;
    private double menorCTempo;

    private JLabel MTV; //MelhorTempoVeiculo
    private JLabel MTP; //MelhorTempoPreço
    private JLabel MTT; //MelhorTempoTempo


    private JLabel MCV; //MelhorCustoVeiculo
    private JLabel MCP; //MelhorCustoPreço
    private JLabel MCT; //MelhorCustoTempo


    private JLabel CBV; //CustoBeneficioVeiculo
    private JLabel CBP; //CustoBeneficioPreço
    private JLabel CBT; //CustoBeneficioTempo

    public JPanel getPanel(){
        return PanelPrincipal;
    }

    public JDialog getDialog(){
        return this;
    }

    //Swingworker para poder construir a tela do EscolhaFrete a partir de um event de um botão.
    public class Loader extends SwingWorker<Object, Object> {

        @Override
        protected Object doInBackground() throws Exception {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    getDialog().setTitle("Escolha uma opção:");
                    getDialog().add(getPanel());
                    getDialog().pack();
                    getDialog().setResizable(false);
                    getDialog().setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                    getDialog().setVisible(true);
                }
            });

            return null;
        }

        @Override
        protected void done() {
            System.out.println("Tela do Frete Controller Construida.");
        }

    }


    public EscolhaFrete(double peso, double tempo, double distancia, double melhorTCusto, double melhorTempo, double maiorCusto, double maiorCTempo, double menorCusto, double menorCTempo) {

        this.peso = peso;
        this.tempo = tempo;
        this.distancia = distancia;
        this.melhorTempo = melhorTempo;
        this.melhorTCusto = melhorTCusto;
        this.maiorCusto = maiorCusto;
        this.maiorCTempo = maiorCTempo;
        this.menorCusto = menorCusto;
        this.menorCTempo = menorCTempo;

        confirmarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double margemLucro = Main.Control.getLucro() / 100;

                String[] Veiculo;

                ArrayList<String> listaDeFretes = new ArrayList();
                String LinhaLida;
                boolean operacaoConcluida;


                try{
                    BufferedReader leitor = new BufferedReader(new FileReader(".\\VeiculosSalvos\\FretesSalvos.txt"));//Lendo todos os fretes que foram salvos no arquivo e adicionando eles no ArryList
                    while ((LinhaLida = leitor.readLine()) != null){
                        listaDeFretes.add(LinhaLida);
                    }
                }catch (Exception err){
                    System.out.println(err);
                }

                //RadialButton de Menor tempo
                if (BMenorTempo.isSelected()) {
                    try {


                        //Adicionando o novo frete na tabela da tela principal.
                        Main.Control.addInTable(MTV.getText(), String.valueOf(distancia), String.valueOf(peso), MTT.getText(), MTP.getText(), String.format("%.2f",menorCusto * margemLucro));

                        //Adicionando o novo frete em um array list para depois ser salvo em um arquivo.
                        listaDeFretes.add(MTV.getText() +"\t"+ ((distancia)+" Km") +"\t"+ ((peso)+" Kg") +"\t"+ MTT.getText() +"\t"+ MTP.getText() +"\t"+ ("R$ "+ String.format("%.2f",menorCusto * margemLucro)));

                        //Separando o MTV o (G) ou (A) de alguns veiculos. para não dar erro na hora de remover da lista de veiculos disponiveis
                        Veiculo = MTV.getText().split(" ");
                        operacaoConcluida = Main.Control.rmInList(Veiculo[0]);//removendo o veiculo da lista de veiculos disponiveis.

                        try {
                            BufferedWriter Writer = new BufferedWriter(new FileWriter(".\\VeiculosSalvos\\FretesSalvos.txt"));//Escrevendo os fretes salvos e o novo frete no arquivo de texto.
                            for(String v : listaDeFretes){

                                Writer.write(v);
                                Writer.newLine();
                            }
                            Writer.close();

                        }catch (Exception err){
                            System.out.println(err);
                        }

                        if(operacaoConcluida){

                            removerDoFile(Veiculo[0]);//Removendo do arquivo que salva a frota de veiculos o veiculo que foi utilizado no frete.

                        }

                        dispose();//fechando a tela.

                    } catch (NumberFormatException err) {
                        System.out.println(err);
                    }

                    //RadialButton de Menor Preço
                    //O mesmo processo do anterior mas com os valores do botão de menor custo.
                } else if (BMenorCusto.isSelected()) {
                    try {
                        Main.Control.addInTable(MCV.getText(), String.valueOf(distancia), String.valueOf(peso), MCT.getText(), MCP.getText(), String.format("%.2f",menorCusto * margemLucro));

                        listaDeFretes.add(MCV.getText() +"\t"+ ((distancia)+" Km") +"\t"+ ((peso)+" Kg") +"\t"+ MCT.getText() +"\t"+ MCP.getText() +"\t"+ ("R$ "+ String.format("%.2f",menorCusto * margemLucro)));

                        Veiculo = MCV.getText().split(" ");

                        operacaoConcluida = Main.Control.rmInList(Veiculo[0]);

                        try {
                            BufferedWriter Writer = new BufferedWriter(new FileWriter(".\\VeiculosSalvos\\FretesSalvos.txt"));

                            for(String v : listaDeFretes){

                                Writer.write(v);
                                Writer.newLine();

                            }
                            Writer.close();

                        }catch (Exception err){

                            System.out.println(err);

                        }

                        if(operacaoConcluida){

                            removerDoFile(Veiculo[0]);

                        }

                        dispose();

                    } catch (NumberFormatException err) {

                        System.out.println(err);

                    }
                    //RadialButton de Maior CustoBeneficio
                    //O mesmo processo do anterior mas com os valores do botão de Maior CustoBeneficio.
                } else if (BCustoBeneficio.isSelected()) {
                    try {

                        Main.Control.addInTable(CBV.getText(), String.valueOf(distancia), String.valueOf(peso), CBT.getText(), CBP.getText(), String.format("%.2f",maiorCusto * margemLucro));

                        listaDeFretes.add(CBV.getText() +"\t"+ ((distancia)+" Km") +"\t"+ ((peso)+" Kg") +"\t"+ CBT.getText() +"\t"+ CBP.getText() +"\t"+ ("R$ "+ String.format("%.2f",maiorCusto * margemLucro)));

                        Veiculo = CBV.getText().split(" ");

                        operacaoConcluida = Main.Control.rmInList(Veiculo[0]);

                        try {

                            BufferedWriter Writer = new BufferedWriter(new FileWriter(".\\VeiculosSalvos\\FretesSalvos.txt"));

                            for(String v : listaDeFretes){

                                Writer.write(v);
                                Writer.newLine();

                            }

                            Writer.close();

                        }catch (Exception err){

                            System.out.println(err);

                        }


                        if(operacaoConcluida){

                            removerDoFile(Veiculo[0]);

                        }

                        dispose();

                    } catch (NumberFormatException err) {

                        System.out.println(err);

                    }
                    //Caso nenhum botão for selecionado mostra uma mensagem de erro e pede para selecionar um dos botões.
                } else {
                    JOptionPane.showMessageDialog(null, "Selecione um tipo de frete.", "Erro", JOptionPane.ERROR_MESSAGE);
                }

            }
        });

    }//Fim do contrutor do EscolhaFrete.

    public void inicializarTela(){
        Loader NewScreen = new Loader();
        NewScreen.run();
    }

    //Função para poder remover do arquivo que salva os veiculos da frota o veiculo que for utilizado no frete.
    public void removerDoFile(String VeiculoSelecionado) {

        ArrayList<String> Veiculos = new ArrayList<>();
        String Linha;

        try {
            BufferedReader Leitor = new BufferedReader(new FileReader(".\\VeiculosSalvos\\Veiculos.txt"));
            while ((Linha = Leitor.readLine()) != null) {
                Veiculos.add(Linha);
            }
            Leitor.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        VeiculoSelecionado += " 1";
        //System.out.println(Veiculos.contains(VeiculoSelecionado));


        if (Veiculos.contains(VeiculoSelecionado)) {//Caso tenha o veiculo na lista de veiculos
            int index = Veiculos.indexOf(VeiculoSelecionado);//Pegar a posição dele.

            //System.out.println(index);

            Veiculos.remove(index);//Remover esse veiculo da lista

            //Escrever a nova frota de veiculos no arquivo.
            try {

                BufferedWriter Writer = new BufferedWriter(new FileWriter(".\\VeiculosSalvos\\Veiculos.txt"));

                for (String v : Veiculos) {

                    Writer.write(v);
                    Writer.newLine();

                }

                Writer.close();

            } catch (Exception e) {

                System.out.println(e);

            }
        }//Fim do if(Veiculos.contains(VeiculoSelecionado)).


    }//Fim da função removerDoFile.


    //Setadores das labels mostradas ao usuario.
    public void setMTV(String NewValue){
        MTV.setText(NewValue);
    }
    public void setMTP(String NewValue){
        MTP.setText(NewValue);
    }
    public void setMTT(String NewValue){
        MTT.setText(NewValue);
    }


    public void setMCV(String NewValue){
        MCV.setText(NewValue);
    }
    public void setMCP(String NewValue){
        MCP.setText(NewValue);
    }
    public void setMCT(String NewValue){
        MCT.setText(NewValue);
    }


    public void setCBV(String NewValue){
        CBV.setText(NewValue);
    }
    public void setCBP(String NewValue){
        CBP.setText(NewValue);
    }
    public void setCBT(String NewValue){
        CBT.setText(NewValue);
    }









}
