package AdicionarFrete;

import EsolhaFrete.EscolhaFrete;
import sistema.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

public class AdFrete extends JDialog{
    //Objetos swing
    private JPanel AdicionarFreteP;
    private JFormattedTextField TextPeso;
    private JFormattedTextField TextDist;
    private JFormattedTextField TextTempo;
    private JButton confirmarButton;

    //Primitivas usadas nos 3 calculos.
    private double peso, distancia, tempo;
    private final double ALCOOL_PRECO = 3.499, GAS_PRECO = 4.499, DIESEL_PRECO = 3.869;

    private double menorCusto = Integer.MAX_VALUE;
    private double menorCTempo;
    private String menorCNome;

    private double maiorCusto = 0;
    private double maiorCTempo;
    private String maiorCNome;

    private double melhorTempo = Integer.MAX_VALUE;
    private double melhorTCusto;
    private String melhorTNome;

    //Geters
    public JPanel getPanel(){
        return AdicionarFreteP;
    }

    public JDialog getDialog(){
        return this;
    }


    //Swingworker para poder construir a tela de AdFrete apartir de um event de um botão.
    public class Loader extends SwingWorker<Object, Object> {

        @Override
        protected Object doInBackground() throws Exception {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    getDialog().setTitle("Adicionar novo frete");
                    getDialog().add(getPanel());
                    getDialog().pack();
                    getDialog().setResizable(false);
                    getDialog().setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                    getDialog().setVisible(true);
                }
            });

            return null;
        }

        @Override
        protected void done() {
            System.out.println("Tela de Adicionar Dados do Frete Construida.");
        }

    }



    public AdFrete() {
        confirmarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                //System.out.println(TextPeso.getText().isBlank());

                if(TextPeso.getText().isBlank() || TextDist.getText().isBlank() || TextTempo.getText().isBlank()){//Caso algum dado esteja faltando mostrar uma janela de erro para o usuario.

                    JOptionPane.showMessageDialog(null, "Insira os dados.", "Error", JOptionPane.ERROR_MESSAGE);//janela de erro.

                } else {

                    try{
                        //Setando as variaveis para os calculos com o dados inseridos pelo usuario
                        peso = Double.parseDouble(TextPeso.getText());
                        distancia = Double.parseDouble(TextDist.getText());
                        tempo = Double.parseDouble(TextTempo.getText());

                        //Veiculo base.
                        Veiculo v;

                        //Setando os valores de caso
                        menorCusto = Integer.MAX_VALUE;
                        maiorCusto = 0;
                        melhorTempo = Integer.MAX_VALUE;


                        //Calculando os preços e tempo para cada tipo de veiculo caso esse veiculo esteja disponivel na frota.
                        if (Main.Control.getCarro() > 0) {
                            v = new Carro();
                            //Calculo com os 2 tipos de combustivel.
                            calcular(v, "Carro (G)", GAS_PRECO);
                            calcular(v, "Carro (A)", ALCOOL_PRECO);
                        }
                        if (Main.Control.getMoto() > 0) {
                            v = new Moto();
                            //Calculo com os 2 tipos de combustivel.
                            calcular(v, "Moto (G)", GAS_PRECO);
                            calcular(v, "Moto (A)", ALCOOL_PRECO);
                        }
                        if (Main.Control.getVan() > 0) {
                            v = new Van();
                            calcular(v, "Van", DIESEL_PRECO);
                        }
                        if (Main.Control.getCarreta() > 0) {
                            v = new Carreta();
                            calcular(v, "Carreta", DIESEL_PRECO);
                        }

                        if (maiorCNome == null) {//Caso apos os calculos não tenha o maior Custo Nome quer dizer que n foi calculado por nenhum veiculo nenhum valor.
                                                 //Logo mostra uma mensagem de erro.

                            JOptionPane.showMessageDialog(null, "Nenhum veículo disponível pode cumprir esse frete.", "Erro", JOptionPane.ERROR_MESSAGE);


                        } else {
                            //Crinado um EscolhaFrete que cuida da escolha do usuario sobre o tipo de frete.
                            EscolhaFrete NovoFrete = new EscolhaFrete(peso, tempo, distancia, melhorTCusto, melhorTempo, maiorCusto, maiorCTempo, menorCusto, menorCTempo);

                            //Setando o formato de escrita do para decimais
                            DecimalFormat df = new DecimalFormat("#.##");

                            //Bloco para setar os labels que vão ser mostrados para o usuario e ele escolher a opção de frete.
                            NovoFrete.setMTV(melhorTNome);
                            NovoFrete.setMTP("R$ " + df.format(melhorTCusto*(1+Main.Control.getLucro()/100)));
                            NovoFrete.setMTT(df.format(melhorTempo) + "h");

                            NovoFrete.setCBV(maiorCNome);
                            NovoFrete.setCBP("R$ " + df.format(maiorCusto*(1+Main.Control.getLucro()/100)));
                            NovoFrete.setCBT(df.format(maiorCTempo) + "h");

                            NovoFrete.setMCV(menorCNome);
                            NovoFrete.setMCP("R$ " + df.format(menorCusto*(1+Main.Control.getLucro()/100)));
                            NovoFrete.setMCT(df.format(menorCTempo) + "h");

                            //Inicializa a tela para escolha do usuario.
                            NovoFrete.inicializarTela();

                            //Fecha a tela de inserir dados para o frete.
                            dispose();
                        }

                    }catch (NumberFormatException e){
                        //Caso o usuario não insira numeros mostre uma mensagem de erro.
                        JOptionPane.showMessageDialog(null, "Insira apenas numeros.", "Error", JOptionPane.ERROR_MESSAGE);
                        //System.out.println("Adicionar frete erro: " + e);
                    }
                }
            }
        });
    }

    //Função para calcular os 3 tipos de fretes possiveis.
    private void calcular(Veiculo v, String nome, double tipoCombustivel) {

        double tempoTotal = distancia / v.getVelMed();

        //System.out.println(nome + " tempo: " + tempoTotal);

        if (peso <= v.getMaxCarg() && tempo >= tempoTotal) {

            double custoMultiplier = distancia / (v.getRendimento() - (peso * v.getRendLoss())); //Multiplayer de quanto se vai gastar na viagem.
            double custoCombustivel = custoMultiplier * tipoCombustivel;//Custo total do frete

            if (custoCombustivel < menorCusto) {//Caso o custo do combustivel seja menor que o menor custo atual (O menor custo pode alterar pois se faz multiplos calculos). Novos valores vão ser atribuidos as variaveis de menorValor.
                menorCusto = custoCombustivel;
                menorCTempo = tempoTotal;
                menorCNome = nome;
            }

            if (custoCombustivel > maiorCusto) {//Caso o custo do combustivel seja maior que o maior Custo atual (O maior Custo pode alterar pois se faz multiplos calculos). Novos valores vão ser atribuidos as variaveis de maiorValor.
                maiorCusto = custoCombustivel;
                maiorCTempo = tempoTotal;
                maiorCNome = nome;
            }

            if (tempoTotal < melhorTempo) {//Caso o tempo que o veiculo x seja menor que o melhorTempo anterior. Novos valores vão ser atribuidos as variaveis de melhorTempo.
                melhorTempo = tempoTotal;
                melhorTCusto = custoCombustivel;
                melhorTNome = nome;
            }
        }
    }

    //Função para criar e iniciar o SwingWorker.
    public void InicializarJanela(){
        Loader NewScreen = new Loader();
        NewScreen.run();
    }

}//Final da classe AdFrete.
