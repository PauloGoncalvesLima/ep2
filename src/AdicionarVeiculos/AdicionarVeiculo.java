package AdicionarVeiculos;

import sistema.Main;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;


//TODO: trocar de JFrame para JDiolog.

public class AdicionarVeiculo {

    private JComboBox Veiculos;
    private JButton adicionarVeiculoButton;
    private JButton cancelarButton;
    private JPanel Mainpanel;
    static private JFrame TelaADVeiculo;

    public AdicionarVeiculo() {
        cancelarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                //Fecahndo o frame.
                TelaADVeiculo.dispose();
            }
        });

        adicionarVeiculoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                String VeiculoSelecionado;

                VeiculoSelecionado = Veiculos.getSelectedItem().toString();//Pegando a seleção do usuario.

                ArrayList<String> Veiculos = new ArrayList<>();
                String Linha;

                try {
                    BufferedReader Leitor = new BufferedReader(new FileReader(".\\VeiculosSalvos\\Veiculos.txt"));
                    while ((Linha = Leitor.readLine()) != null){
                        Veiculos.add(Linha);
                    }
                }catch (Exception e){
                    System.out.println(e);
                }

                Veiculos.add(VeiculoSelecionado + " 1");

                try {
                    BufferedWriter Writer = new BufferedWriter(new FileWriter(".\\VeiculosSalvos\\Veiculos.txt"));
                    for(String v : Veiculos){
                        Writer.write(v);
                        Writer.newLine();
                    }
                    Writer.close();
                }catch (Exception e){
                    System.out.println(e);
                }
                Main.Control.addInList(VeiculoSelecionado);
            }
        });
    }

    public void InicializarJanela(){


        TelaADVeiculo = new JFrame("Adicionar Veiculo");
        AdicionarVeiculo ADveiculo = new AdicionarVeiculo();

        TelaADVeiculo.setContentPane(ADveiculo.getMainpanel());
        TelaADVeiculo.pack();
        TelaADVeiculo.setResizable(false);
        TelaADVeiculo.setVisible(true);

    }


    public JPanel getMainpanel() {
        return Mainpanel;
    }
}
