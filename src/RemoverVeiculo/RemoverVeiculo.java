package RemoverVeiculo;

import sistema.Main;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class RemoverVeiculo {

    private JComboBox Rmveiculo;
    private JButton removerVeiculoButton;
    private JButton cancelarButton;
    private JPanel Remover;
    static private JFrame RMVeic;

    public RemoverVeiculo() {
        removerVeiculoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String VeiculoSelecionado;
                boolean operacaoConcluida;

                VeiculoSelecionado = Rmveiculo.getSelectedItem().toString();

                //System.out.println(VeiculoSelecionado);

                operacaoConcluida = Main.Control.rmInList(VeiculoSelecionado);

                if(operacaoConcluida){

                    ArrayList<String> Veiculos = new ArrayList<>();
                    String Linha;

                    try {


                        BufferedReader Leitor = new BufferedReader(new FileReader(".\\VeiculosSalvos\\Veiculos.txt"));

                        while ((Linha = Leitor.readLine()) != null){

                            Veiculos.add(Linha);

                        }
                        Leitor.close();


                    }catch (Exception e){

                        System.out.println(e);

                    }

                    VeiculoSelecionado+=" 1";

                    //System.out.println(Veiculos.contains(VeiculoSelecionado));

                    if(Veiculos.contains(VeiculoSelecionado)){

                        int index = Veiculos.indexOf(VeiculoSelecionado);

                        //System.out.println(index);

                        Veiculos.remove(index);

                        try {

                            BufferedWriter Writer = new BufferedWriter(new FileWriter(".\\VeiculosSalvos\\Veiculos.txt"));

                            for(String v : Veiculos){
                                Writer.write(v);
                                Writer.newLine();
                            }

                            Writer.close();

                        }catch (Exception e){

                            System.out.println(e);

                        }

                    }

                }

                if(!operacaoConcluida){

                    JOptionPane.showMessageDialog(null, "Não foi possivel remover o "+ VeiculoSelecionado + "", "Erro: ", JOptionPane.ERROR_MESSAGE);

                }
            }
        });
        cancelarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                RMVeic.dispose();
            }
        });
    }


    public void InicializarJanela(){

        RMVeic = new JFrame("Remover Veiculo");
        RemoverVeiculo rmVeiculo = new RemoverVeiculo();

        RMVeic.setContentPane(rmVeiculo.getMainpanel());
        RMVeic.pack();
        RMVeic.setResizable(false);

        RMVeic.setVisible(true);

    }

    public JPanel getMainpanel(){
        return Remover;
    }
}
